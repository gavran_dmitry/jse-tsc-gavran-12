package ru.tsc.gavran.tm.api.service;

import ru.tsc.gavran.tm.enumerated.Status;
import ru.tsc.gavran.tm.model.Project;

import java.util.List;

public interface IProjectService {

    void create(String name);

    void create(String name, String description);

    void add(Project project);

    void remove(Project project);

    List<Project> findAll();

    Project findById(String id);

    Project findByName(String name);

    Project findByIndex(Integer index);

    Project removeById(String id);

    Project removeByName(String name);

    Project removeByIndex(Integer index);

    Project updateByIndex(Integer index, String name, String description);

    Project updateById(String id, String name, String description);

    Project startById(String id);

    Project startByIndex(Integer index);

    Project startByName(String name);

    Project finishById(String id);

    Project finishByIndex(Integer index);

    Project finishByName(String name);

    Project changeStatusById(String id, Status status);

    Project changeStatusByName(String name, Status status);

    Project changeStatusByIndex(Integer index, Status status);

    void clear();

}